import {
    RECEIVE_DATA,
    SET_MENU,
    SET_SIZE,
    SET_SHOW,
    SET_START,
    SET_END,
    SET_FROM,
    SET_TO,
    SET_INSTRUMENTS,
    SET_RESULTS,
    SET_MINMAX,
    SET_VALUES
} from "./allActions";
import { maxBy, minBy, max, min } from "lodash-es";
import Data from "../data/mktdata.json";
export function setInstruments(instruments) {
    return { type: SET_INSTRUMENTS, instruments };
}
export function receiveData(data) {
    return { type: RECEIVE_DATA, data };
}
export function setValues(values) {
    return { type: SET_VALUES, values };
}
export function setFrom(from) {
    return { type: SET_FROM, from };
}
export function setTo(to) {
    return { type: SET_TO, to };
}
export function setMinMax(minMax) {
    return { type: SET_MINMAX, minMax };
}
export function receiveInfo(info) {
    info.forEach(item => {
        item.entries = item.timeSeries.entries;
        delete item.timeSeries;
    });
    let valMax = info.map(item => {
        let maxi = maxBy(item.entries, "v");
        return maxi.v;
    });
    valMax = max(valMax);
    let valMin = info.map(item => {
        let mini = minBy(item.entries, "v");
        return mini.v;
    });
    valMin = min(valMin);
    const mm = info[0].entries.reduce((a, b) => {
        let val = typeof a === "string" ? a : a.d;
        return new Date(val) < new Date(b.d) ? val : b.d;
    });
    const mx = info[0].entries.reduce((a, b) => {
        let val = typeof a === "string" ? a : a.d;
        return new Date(val) > new Date(b.d) ? val : b.d;
    });
    let k = {};
    info.forEach(it => {
        k[it.instrumentId] = true;
    });
    return dispatch => {
        dispatch(setValues([valMin, valMax]));
        dispatch(setMinMax([valMin, valMax]));
        dispatch(receiveData(info));
        dispatch(setInstruments(k));
        dispatch(setFrom(new Date(mm)));
        dispatch(setTo(new Date(mx)));
        dispatch(setStart(new Date(mm)));
        dispatch(setEnd(new Date(mx)));
    };
}

export function filtra(ini, end, ins, ele, values) {
    let results = ele.filter(item => {
        return ins[item.instrumentId];
    });
     const n = results.map((r, i) => {
        const res = r.entries.filter(item => {
            return (
                new Date(item.d) >= new Date(ini) &&
                new Date(item.d) <= new Date(end) &&
                item.v >= values[0] &&
                item.v <= values[1]
            );
        });

        return Object.assign({}, results[i], { entries: res });
    });
    return { type: SET_RESULTS, results: n };
}

export function updateInstruments(all, inst, items, start, end, values) {
    all[inst] = !all[inst];
    let newInst = Object.assign({}, all);
    return dispatch => {
        dispatch(setInstruments(newInst));
        dispatch(filtra(start, end, newInst, items, values));
    };
}
export function updateSize(width) {
    // Update size for graph responsiveness
    return { type: SET_SIZE, width: width };
}
export function setMenu(menu) {
    return { type: SET_MENU, menu: !menu };
}
export function setShow(val) {
    return { type: SET_SHOW, show: val };
}
export function fetchData() {
    return dispatch => {
        dispatch(receiveInfo(Data.mktData));
    };
}
export function setStart(start) {
    return { type: SET_START, start };
}
export function setEnd(end) {
    return { type: SET_END, end };
}
