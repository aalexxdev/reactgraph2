import React, { Component } from "react";
import FormGroup from "@material-ui/core/FormGroup";
import FormControlLabel from "@material-ui/core/FormControlLabel";
import Switch from "@material-ui/core/Switch";

class Selections extends Component {
    constructor(props) {
        super(props);
        this.handleChange = this.handleChange.bind(this);
    }
    handleChange(e) {
        this.props.appActions.updateInstruments(
            this.props.instruments,
            e.target.value,
            this.props.data,
            this.props.start,
            this.props.end,
            this.props.values
        );
    }
    isDisabled(who) {
        // Disable switch if only 1 graph active
        return (
            Object.values(this.props.instruments).filter(i => {
                return i;
            }).length <= 1 && this.props.instruments[who]
        );
    }
    render() {
        return (
            <FormGroup row>
                {Object.keys(this.props.instruments).length > 0 &&
                    Object.keys(this.props.instruments).map(inst => {
                        return (
                            <FormControlLabel
                                className="swit"
                                key={inst}
                                control={
                                    <Switch
                                        checked={this.props.instruments[inst]}
                                        onChange={e => {
                                            this.handleChange(e);
                                        }}
                                        value={inst}
                                        color="primary"
                                        disabled={this.isDisabled(inst)}
                                    />
                                }
                                label={inst}
                            />
                        );
                    })}
            </FormGroup>
        );
    }
}
export default Selections;
