import React, { Component } from "react";
import moment from "moment";
import Helmet from "react-helmet";

import DayPickerInput from "react-day-picker/DayPickerInput";
import "react-day-picker/lib/style.css";

import { formatDate, parseDate } from "moment";

class Calendar extends Component {
    constructor(props) {
        super(props);
        this.handleFromChange = this.handleFromChange.bind(this);
        this.handleToChange = this.handleToChange.bind(this);
        this.state = {
            from: undefined,
            to: undefined
        };
    }
    showFromMonth() {
        const { from, to } = this.state;
        if (!from) {
            return;
        }
        if (moment(to).diff(moment(from), "months") < 2) {
            this.to.getDayPicker().showMonth(from);
        }
    }
    handleFromChange(from) {
        this.setState({ from });
        this.props.appActions.setStart(moment(from).format("YYYY-MM-DD"));

        this.props.appActions.filtra(
            moment(from).format("YYYY-MM-DD"),
            this.props.end,
            this.props.instruments,
            this.props.data,
            this.props.values
        );
    }
    handleToChange(to) {
        this.setState({ to }, this.showFromMonth);
        this.props.appActions.setEnd(moment(to).format("YYYY-MM-DD"));
        this.props.appActions.filtra(
            this.props.start,
            moment(to).format("YYYY-MM-DD"),
            this.props.instruments,
            this.props.data,
            this.props.values
        );
    }
    componentWillMount() {
        let f = new Date(this.props.from);
        let t = new Date(this.props.to);
        this.setState({ from: f, to: t });
    }
    render() {
        const { from, to } = this.state;
        const modifiers = { start: from, end: to };
        return (
            <div className="InputFromTo">
                From{" "}
                <DayPickerInput
                    value={from}
                    placeholder="From"
                    format="LL"
                    formatDate={formatDate}
                    parseDate={parseDate}
                    dayPickerProps={{
                        selectedDays: [from, { from, to }],
                        disabledDays: {
                            after: this.props.to,
                            before: this.props.from
                        },
                        toMonth: to,
                        modifiers,
                        numberOfMonths: 2,
                        onDayClick: () => this.to.getInput().focus()
                    }}
                    onDayChange={this.handleFromChange}
                />{" "}
                To
                <span className="InputFromTo-to">
                    <DayPickerInput
                        ref={el => (this.to = el)}
                        value={to}
                        placeholder="To"
                        format="LL"
                        formatDate={formatDate}
                        parseDate={parseDate}
                        dayPickerProps={{
                            selectedDays: [from, { from, to }],
                            disabledDays: {
                                before: this.props.from,
                                after: this.props.to
                            },
                            modifiers,
                            month: from,
                            fromMonth: from,
                            numberOfMonths: 2
                        }}
                        onDayChange={this.handleToChange}
                    />
                </span>
                <Helmet>
                    <style>{`
  .InputFromTo .DayPicker-Day--selected:not(.DayPicker-Day--start):not(.DayPicker-Day--end):not(.DayPicker-Day--outside) {
    background-color: #f0f8ff !important;
    color: #4a90e2;
  }
  .InputFromTo .DayPicker-Day {
    border-radius: 0 !important;
  }
  .InputFromTo .DayPicker-Day--start {
    border-top-left-radius: 50% !important;
    border-bottom-left-radius: 50% !important;
  }
  .InputFromTo .DayPicker-Day--end {
    border-top-right-radius: 50% !important;
    border-bottom-right-radius: 50% !important;
  }
  .InputFromTo .DayPickerInput-Overlay {
    width: 550px;
  }
  .InputFromTo-to .DayPickerInput-Overlay {
    margin-left: -198px;
  }
`}</style>
                </Helmet>
            </div>
        );
    }
}
export default Calendar;
