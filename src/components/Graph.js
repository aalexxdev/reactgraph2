import React, { Component } from "react";
import Divsize from "./Divsize";
import {
    LineChart,
    XAxis,
    YAxis,
    Tooltip,
    CartesianGrid,
    Legend,
    Line
} from "recharts";

class Graph extends Component {
    render() {
        const { items } = this.props;
        let colors = {
            70: "#FFCC00",
            71: "#8F87FF",
            72: "#C000FF",
            75: "#FFEF44",
            78: "#FF579B",
            152: "#ADFF58"
        };

        if (items.length > 0) {
            // limit to 600 for testing performance
            let parsed = items.map(item => {
                if (item.entries.length >= 600) {
                    item.entries.length = 600;
                }
                return item;
            });
            return (
                <div>
                    <Divsize {...this.props} />
                    <LineChart width={this.props.width} height={300}>
                        <CartesianGrid strokeDasharray="3 3" />
                        <XAxis
                            dataKey="d"
                            allowDuplicatedCategory={false}
                            type="category"
                        />
                        <YAxis dataKey="v" />
                        <Tooltip />
                        <Legend />
                        {parsed.map((dat, i) => (
                            <Line
                                dot={false}
                                type="monotone"
                                dataKey="v"
                                stroke={colors[dat.instrumentId]}
                                data={dat.entries}
                                name={dat.instrumentId}
                                key={dat.instrumentId}
                            />
                        ))}
                    </LineChart>
                </div>
            );
        } else {
            return <div>No results...</div>;
        }
    }
}
export default Graph;
