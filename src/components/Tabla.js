import React, { Component } from "react";
import BootstrapTable from "react-bootstrap-table-next";
import "react-bootstrap-table-next/dist/react-bootstrap-table2.min.css";
import paginationFactory from "react-bootstrap-table2-paginator";
class Tabla extends Component {
    render() {
        if (
            this.props.items.length > 0 &&
            typeof this.props.items[0].entries !== undefined &&
            this.props.items[0].entries.length > 0
        ) {
            let parsed = [];
            let instruments = [];
            this.props.items.forEach(el => {
                instruments.push(el.instrumentId);
            });
            this.props.items[0].entries.forEach(dat => {
                let date = dat.d;
                let obj = { date };
                instruments.forEach((ins, i) => {
                    let which = this.props.items[i].entries.find(
                        fi => fi.d === date
                    );
                    try {
                        obj[ins] = which.v.toFixed(2);
                    } catch (error) {
                        obj[ins] = "N/A";
                    }
                });
                parsed.push(obj);
            });
            const columns = Object.keys(parsed[0]).map(item => {
                return {
                    dataField: item,
                    text: item.replace(/\b\w/g, function(l) {
                        return l.toUpperCase();
                    }),
                    sort: true,
                    align: "center",
                    title: true
                };
            });
            const options = {
                currSizePerPage: 9,
                sizePerPageList: [
                    {
                        text: "9",
                        value: 9
                    }
                ]
            };
            return (
                <BootstrapTable
                    striped
                    hover
                    condensed
                    keyField="date"
                    data={parsed}
                    columns={columns}
                    pagination={paginationFactory(options)}
                    defaultSortDirection="asc"
                />
            );
        } else {
            return <div>'No results...'</div>;
        }
    }
}

export default Tabla;
