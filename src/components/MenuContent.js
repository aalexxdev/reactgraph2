import React from "react";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import ShowChart from "@material-ui/icons/ShowChart";
import TableChart from "@material-ui/icons/TableChart";
import AllInclusive from "@material-ui/icons/AllInclusive";
import Divider from "@material-ui/core/Divider";
class MenuContent extends React.Component {
    render() {
        return (
            <div>
                <ListItem
                    button
                    onClick={() => this.props.appActions.setShow(1)}
                >
                    <ListItemIcon>
                        <ShowChart />
                    </ListItemIcon>
                    <ListItemText primary="Chart" />
                </ListItem>
                <ListItem
                    button
                    onClick={() => this.props.appActions.setShow(2)}
                >
                    <ListItemIcon>
                        <TableChart />
                    </ListItemIcon>
                    <ListItemText primary="Table" />
                </ListItem>
                <Divider />
                <ListItem
                    button
                    onClick={() => this.props.appActions.setShow(0)}
                >
                    <ListItemIcon>
                        <AllInclusive />
                    </ListItemIcon>
                    <ListItemText primary="All" />
                </ListItem>
            </div>
        );
    }
}
export default MenuContent;
