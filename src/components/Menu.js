import React from "react";
import PropTypes from "prop-types";
import { withStyles } from "@material-ui/core/styles";
import SwipeableDrawer from "@material-ui/core/SwipeableDrawer";

import MenuContent from "./MenuContent";

const styles = {
    list: {
        width: 250
    },
    fullList: {
        width: "auto"
    }
};

class Menu extends React.Component {
    state = {
        top: false,
        left: false,
        bottom: false,
        right: false
    };

    toggleDrawer = (side, open) => () => {
        this.props.appActions.setMenu(this.props.menu);
    };

    render() {
        return (
            <div>
                <SwipeableDrawer
                    open={this.props.menu}
                    onClose={this.toggleDrawer("left", false)}
                    onOpen={this.toggleDrawer("left", true)}
                >
                    <div
                        tabIndex={0}
                        role="button"
                        onClick={this.toggleDrawer("left", false)}
                        onKeyDown={this.toggleDrawer("left", false)}
                    >
                        <MenuContent {...this.props} />
                    </div>
                </SwipeableDrawer>
            </div>
        );
    }
}

Menu.propTypes = {
    classes: PropTypes.object.isRequired
};

export default withStyles(styles)(Menu);
