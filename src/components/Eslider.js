import "rc-slider/assets/index.css";
import React from "react";
import Slider from "rc-slider";
const createSliderWithTooltip = Slider.createSliderWithTooltip;
const Range = createSliderWithTooltip(Slider.Range);

export default class Eslider extends React.Component {
    constructor(props) {
        super(props);
        this.onSliderChange = this.onSliderChange.bind(this);
    }
    onSliderChange(values) {
        this.props.appActions.setValues(values);
        this.props.appActions.filtra(
            this.props.start,
            this.props.end,
            this.props.instruments,
            this.props.data,
            values
        );
    }
    render() {
        return (
            <div style={{ paddingTop: "12px" }}>
                <Range
                    tipFormatter={values => `${parseFloat(values, 2)}`}
                    defaultValue={this.props.values}
                    min={this.props.minMax[0]}
                    max={this.props.minMax[1]}
                    allowCross={false}
                    value={this.props.values}
                    onChange={values => {
                        this.onSliderChange(values);
                    }}
                />
            </div>
        );
    }
}
