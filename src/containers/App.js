import React, { Component } from "react";
import "./App.css";
import Header from "../components/Header";
import Graph from "../components/Graph";
import Tabla from "../components/Tabla";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import PropTypes from "prop-types";
import compose from "recompose/compose";
import { withStyles } from "@material-ui/core/styles";
import * as appActions from "../actions/appActions";
import Paper from "@material-ui/core/Paper";
import Grid from "@material-ui/core/Grid";
import Menu from "../components/Menu";
import Calendar from "../components/Calendar";
import Selections from "../components/Selections";
import Eslider from "../components/Eslider";
const styles = theme => ({
    root: {
        flexGrow: 1
    },
    cont: {
        flexGrow: 1,
        margin: 15
    },
    filter: { margin: 15 },
    paper: {
        padding: theme.spacing.unit * 2,
        textAlign: "center",
        color: theme.palette.text.secondary
    }
});
class App extends Component {
    componentWillMount() {
        this.props.appActions.fetchData();
    }
    render() {
        const { cont, paper, filter } = this.props.classes;
        const { show, appActions, menu, width } = this.props;
        let items = [];
        if (
            JSON.stringify(this.props.start) ===
                JSON.stringify(this.props.from) &&
            JSON.stringify(this.props.end) === JSON.stringify(this.props.to) &&
            Object.values(this.props.instruments).indexOf(false) === -1 &&
            this.props.minMax[0] === this.props.values[0] &&
            this.props.minMax[1] === this.props.values[1]
        ) {
            items = this.props.data;
        } else {
            items = this.props.results;
        }
        return (
            <div className="App">
                <Header
                    show={show}
                    appActions={appActions}
                    data={items}
                    menu={menu}
                    width={width}
                />

                <Menu
                    show={show}
                    appActions={appActions}
                    data={items}
                    menu={menu}
                    width={width}
                />
                <Grid container spacing={24}>
                    <Grid className={filter} item xs={12} sm={12} lg={3} md={3}>
                        {this.props.from !== "" && (
                            <Calendar {...this.props} items={items} />
                        )}
                    </Grid>
                    <Grid item xs={12} sm={12} lg={3} md={3}>
                        <Selections {...this.props} items={items} />
                    </Grid>
                    <Grid item xs={12} sm={12} lg={3} md={3}>
                        <Eslider {...this.props} />
                    </Grid>
                </Grid>

                {this.props.data.length > 0 && (
                    <div className={cont}>
                        <Grid container spacing={24}>
                            {(show === 0 || show === 1) && (
                                <Grid
                                    item
                                    xs={12}
                                    sm={12}
                                    md={show === 1 ? 12 : 6}
                                    lg={show === 1 ? 12 : 6}
                                >
                                    <Paper className={paper}>
                                        <Graph {...this.props} items={items} />
                                    </Paper>
                                </Grid>
                            )}
                            {(show === 0 || show === 2) && (
                                <Grid
                                    item
                                    xs={12}
                                    sm={12}
                                    md={show === 2 ? 12 : 6}
                                    lg={show === 2 ? 12 : 6}
                                >
                                    <Paper className={paper}>
                                        {" "}
                                        <Tabla {...this.props} items={items} />
                                    </Paper>
                                </Grid>
                            )}
                        </Grid>
                    </div>
                )}
                {this.props.data.length <= 0 && (
                    <div>Un momento por favor...</div>
                )}
            </div>
        );
    }
}

function mapStateToProps(state) {
    return {
        data: state.appRed.data,
        menu: state.appRed.menu,
        width: state.appRed.width,
        show: state.appRed.show,
        start: state.appRed.start,
        end: state.appRed.end,
        from: state.appRed.from,
        to: state.appRed.to,
        instruments: state.appRed.instruments,
        results: state.appRed.results,
        minMax: state.appRed.minMax,
        values: state.appRed.values
    };
}

function mapDispatchToProps(dispatch) {
    return {
        appActions: bindActionCreators(appActions, dispatch)
    };
}
App.propTypes = {
    classes: PropTypes.object.isRequired
};

export default compose(
    withStyles(styles, { name: "App" }),
    connect(
        mapStateToProps,
        mapDispatchToProps
    )
)(App);
