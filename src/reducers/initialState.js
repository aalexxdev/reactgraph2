export default {
    data: [],
    menu: false,
    width: null,
    show: 0,
    start: "",
    end: "",
    from: "",
    to: "",
    instruments: [],
    results: [],
    minMax: [],
    values: []
};
