import initialState from "./initialState";
import {
    FETCH_DATA,
    RECEIVE_DATA,
    SET_MENU,
    SET_SIZE,
    SET_SHOW,
    SET_START,
    SET_END,
    SET_FROM,
    SET_TO,
    SET_INSTRUMENTS,
    SET_RESULTS,
    SET_MINMAX,
    SET_VALUES
} from "../actions/allActions";

export default function appRed(state = initialState, action) {
    switch (action.type) {
        case FETCH_DATA:
            return action;
        case SET_MINMAX:
            return { ...state, minMax: action.minMax };
        case SET_VALUES:
            return { ...state, values: action.values };
        case RECEIVE_DATA:
            return { ...state, data: action.data };
        case SET_MENU:
            return { ...state, menu: action.menu };
        case SET_SIZE:
            return { ...state, width: action.width };
        case SET_SHOW:
            return { ...state, show: action.show };
        case SET_START:
            return { ...state, start: action.start };
        case SET_RESULTS:
            return { ...state, results: action.results };
        case SET_END:
            return { ...state, end: action.end };
        case SET_FROM:
            return { ...state, from: action.from };
        case SET_TO:
            return { ...state, to: action.to };
        case SET_INSTRUMENTS:
            return { ...state, instruments: action.instruments };
        default:
            return state;
    }
}
