# React Graph


This app shows table and graphs data from mock.

Developed in React / Redux, with Material-UI.


##Installation and run  

Clone repository, install with Yarn and run!  
```
git clone https://bitbucket.org/aalexx1978/reactgraph2.git  
cd reactgraph2
yarn install
yarn start
```  


## Demo on desktop computer  
![demo 2](./assets/graph.gif "Demo on PC")